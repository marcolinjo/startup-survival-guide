# Startup Survival Guide

## Was ist das hier?

Das ist der Guide, den wir selber gern gehabt hätten aber nicht fanden, als wir sie brauchten. 
Es sind die Lessons Learned, die die sexy Veranstaltungen offen gelassen haben.

## Wie kannst Du es nutzen? 

Gern kannst Du unsere Erfahrungen lesen und Deine eigenen Schlüsse daraus ziehen!

Bitte beachte dabei, dass es sich hierbei nur um unsere persönlichen Erfahrungen und Tips an uns selber handelt.
Wir geben diese nach bestem Wissen und Gewissen.
Die Verantwortung für die Nutzung und Dein Leben liegt weiterhin bei Dir. 

Zusätzlich kannst Du gern Deine Erfahrungen zu diesen Themen mit uns teilen!

# Was ist hier enthalten?

* Steuererklärung 101 - Der holprige Weg zu meiner ersten Steuererklärung
* 