# 101 Die erste Steuererklärung mit Startup

## Für wen diese Beschreibung ist?

Es ist der *31.07.20XX* oder sogar noch später im Jahr? Von Deinen Freunden hast Du erfahren, dass die Steuererklärung fällig ist? Mist. Da war ja was? Sofort schießen dir die Worte des freundlichen Mitarbeiters in die Ohren als du vor einigen Monaten im totalen Startuprausch dein Gewerbe angemeldet hast: "Das verpflichtet sie zur Abgabe eine Steuererklärung." Weil du ein Fuchs bist, hatte du die schlanke *Kleinunternehmer-Regelung* gewählt. Um dich in den ersten ulta-leanen Schritten beim Retten der Welt ja nicht von vollständigen Buchhaltung abhalten zu lassen. <br>
Aber selbst diese kleine regulatorische Anforderung hast du mal so richtig verkackt. <br>
Wenn dem so ist, bist du nicht allein. Das muss wohl so sein?
<br><br>
Dies ist das Into, dass ich gern gehabt hätte, als es mir genauso ging. Es hilft dir, in wenigen Schritten zu deiner *ersten Steuererklärung* zu kommen, obwohl (oder vielleicht gerade weil) du genauso steuerlich unerfahren und unmotiviert bist, wie ich.

## Panik - Der Stichtag ist um, und nun?

Erster Reflex - Googeln! Meine ersten Versuche haben mich leider nicht wirklich weiter gebracht. Einen Steuerberater kann ich mir nicht leisten. Ne Software wollte ich mir nicht leisten. Auch die wahrscheinlich mega supidupi Hilfsangebote anderer Startups wie [Wundertax](https://www.steuererklaerung.de/ratgeber-steuern/kleinunternehmer) haben mir in der Sache nicht wirklich geholfen. Auch wenn ich deren geschickten Aufbau der Webseite anerkenne! Du landest halt immer beim Ausprobieren des Produktes. 
Mein Kumpel M. wurde hier gerade am Telefon noch deutlicher: "Danke für nichts." 
OK, zurück zur Lage:

Der Stichtag für die Steuererklärung von dir und deinem Startup ist der *31.07. eines jeden Jahres* (in Braunschweig, heute). Das kannst du dir gleich mal als wiederkehrende Erinnerung in dein <a href="https://www.fairphone.com/de/">Fairphone</a> nageln. Am besten gleich für den Monat vorher. Yes. Nächstes Mal rockst du den Termin. 
OK was ist jetzt wichtig? Wir haben vier Ziele:
1. Möglich negative Konsequenzen unseres Versäumnisses abfangen
2. Zeit für die Steuererklärung gewinnen
3. Verstehen, was der Staat hier von uns will
4. Diese verdammte Steuererklärung abliefern
5. So viel Geld wir möglich dabei zurückholen

## Die Vorbereitung

Ja die Digitalisierung macht auch vor der Steuererklärung nicht halt. Die Finanzämter haben Ihre Daten anscheinend in einem Portal names [Elster](https://www.elster.de/eportal/) zusammengefahren. Dafür brauchen wir wohl erstmal einen Zugang...


## Triff die Profis - Teil 1.

Frag Deine Kollegen und Freunde, welches Finanzamt für Dich zuständig ist. Der Abrechner im Personalbereich deines aktuellen Arbeitgebers weiss sowas meistens. Vielleicht hast du auch irgendwo vielleicht ein Schreiben dieses Finanzamtes in deiner losen Blattsammlung? Aber das ist ganz sicher verstaubt und du hast ja auch diese Bürokratieallergie? Also frag dich durch, auch ältere Kollegen wissen sowas immer. Im schlimmsten Fall kostet es dich einen Espresso. Und Zack! - Du hast die Namen und mit [Google](http://www.google.de), die [Adresse und die Öffnungszeiten](https://lstn.niedersachsen.de/steuer/finanzaemter/finanzamt-braunschweig-wilhelmstrae-67792.html).

Meistens lassen sich Probleme am Besten lösen, wenn die Beteiligten darüber reden: Bla, bla, bla,... ist aber so! Also triff doch mal deinen zuständigen Sachbearbeiter.
Das schafft Verständnis für beide Seiten, und für Dich - Aufschub. Im wesentlichen ist es das wofür du hier bist.
Dabei gibt es in deinem Fall sogar wahrscheinlich zwei Mitarbeiter. Einer ist für deine private Steuererklärung und einer für dein kleines Startup da.
Am besten geht Du zu beiden und legst die Karten auf den Tisch: Du bist zu spät. Du hast keine Ahnung. Und du brauchst nen Intro.  
Und? Hattest Du erfolg? In meinem Fall war es echt Easy. Die beiden waren echt mega hilfsbereit und verständnisvoll. Ich habe einen Aufschub bis September bekommen! Danke nochmal dafür Jungs und Mädels!

## Der miese Teil

Ok. Lass es uns offen aussprechen. Jetzt kommt der miese Teil. Das was keinen Spass macht. Seit du alleine wohnst, hast du alle deine Briefwechsel und Rechnungen in einem wohlüberlegtem zugriffszeitenoptimierten Ablagesystem hinterlegt, welches sogar Aufbewahrungsfristen berücksichtigt? Das ganze hast du für dein Startup und für dich privat? Nein? Ich auch nicht. Eher so: Papier willkührlich durcheinander auf unterschiedlichen Haufen, in Schubladen und Beuteln mit Briefen teilweise ungeöffnet? Verstehe... Ok wir müssen da jetzt ran.
Bei mir hat das einige Tage gedauert. Einen unsortierten Haufen vorn, viele thematisch sortierte Haufen um mich rum und eine Haufen Müll hinter mir. 
Der Spass hat mich mehrere Tage gekostet... meiner kostbaren Lebenzeit. Und vergessen wir nicht, es ist Sommer.
Folgende Haufen sollten mindestens bei rauskommen:
- Arbeitgeber (und die zugehörigen Lohnsteuerbescheinigungen)
- Versicherungen (die kann man sich wohl zum Teil anrechnen lassen) 
- Startup (Genehmigungen, Einnahmen und Ausgaben dafür)
- Müll (Alle Briefwechsel)

Sicher gibt es einen Haufen weiterer sinnvoller Ordner